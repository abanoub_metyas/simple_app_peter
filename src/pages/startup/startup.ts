import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the StartupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-startup',
    templateUrl: 'startup.html',
})
export class StartupPage {

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad StartupPage');
    }

    go_to_login(){
        this.navCtrl.push('LoginPage');
    }

    about_us(){
        this.navCtrl.push('AboutusPage');
    }

    faqs(){
        this.navCtrl.push('FaqsPage');
    }

    contact_us(){
        this.navCtrl.push('ContactusPage');
    }

    calc(){
        this.navCtrl.push('CalcPage');
    }





}
