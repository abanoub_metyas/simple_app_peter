import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalcResPage } from './calc-res';

@NgModule({
  declarations: [
    CalcResPage,
  ],
  imports: [
    IonicPageModule.forChild(CalcResPage),
  ],
})
export class CalcResPageModule {}
