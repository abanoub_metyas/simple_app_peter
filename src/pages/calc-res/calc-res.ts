import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';


@IonicPage()
@Component({
    selector: 'page-calc-res',
    templateUrl: 'calc-res.html',
})
export class CalcResPage {

    public annual_salary:any;
    public financial_year:string;
    public tax_plan:any;
    public select_tab_plan:any;

    public tax_percent_arr:any=[];
    public show_text_arr:any=[];
    public total_tax=0;

    public show_res_as:any=1;
    //annual =>1
    //monthly =>12
    //fortnightly =>26
    //weekly =>52
    public medicare_levy_payable;


    constructor(
        public navCtrl: NavController,
        public navParams: NavParams
    ) {

        this.annual_salary=this.navParams.get("annual_salary");
        this.financial_year=this.navParams.get("financial_year");

        this.annual_salary=parseInt(this.annual_salary);

        // this.annual_salary=1000000;
        // this.financial_year='2018_2019';

        this.tax_plan={
            '2018_2019':{
                '1_18200':'0',
                '18201_37000':'0.19',
                '37001_90000':'0.325',
                '90001_180000':'0.37',
                '180001_*':'0.45',
            },
            '2017_2018':{
                '1_18200':'0',
                '18201_37000':'0.19',
                '37001_87000':'0.325',
                '87001_180000':'0.37',
                '180001_*':'0.45',
            },
            '2016_2017':{
                '1_18200':'0',
                '18201_37000':'0.19',
                '37001_87000':'0.325',
                '87001_180000':'0.37',
                '180001_*':'0.47',
            }


        };
    }

    ionViewDidLoad() {

        if(
            this.annual_salary==NaN||
            this.financial_year==undefined||
            typeof this.tax_plan[this.financial_year]==undefined
        ){
            return false;
        }

        this.select_tab_plan=this.tax_plan[this.financial_year];

        let item_tax_percent:any=0;
        let item_range:any;
        let apply_tax_at_amount=0;

        for (let tax_range of Object.keys(this.select_tab_plan)){

            //check if this.annual_salary between range or not
            item_range=tax_range.split('_');

            if(item_range[1]=="*"){
                item_range[1]=this.annual_salary;
            }

            if(
                !(this.annual_salary>=item_range[0])
            ){
                break;
            }

            item_tax_percent=this.select_tab_plan[tax_range];

            if(this.annual_salary>=item_range[1]){
                apply_tax_at_amount=item_range[1]-item_range[0];
            }
            else{
                apply_tax_at_amount=this.annual_salary-item_range[0];
            }


            this.total_tax+=Math.round(apply_tax_at_amount*item_tax_percent);
            this.tax_percent_arr.push(Math.round(apply_tax_at_amount*item_tax_percent));

            if(item_tax_percent>0){
                this.show_text_arr.push((item_tax_percent*100)+"c for every dollar between $"+item_range[0]+" - $"+item_range[1]);
            }
            else{
                this.show_text_arr.push("no tax on income between $"+item_range[0]+" - $"+item_range[1]);
            }

        }


        this.update_values();


    }

    math_round(digit){
        return Math.round(digit);
    }

    update_values(){
        if(this.total_tax>0){
            this.medicare_levy_payable=this.annual_salary/(50*this.show_res_as);
        }
        else{
            this.medicare_levy_payable=0;
        }
    }


}
