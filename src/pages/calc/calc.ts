import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams,AlertController} from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-calc',
    templateUrl: 'calc.html',
})
export class CalcPage {

    public emp_income:any;
    public emp_extra_income:any;

    public emp_income_type:string="annually";
    public emp_extra_income_type:string="annually";

    public financial_year:string="2018_2019";

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public alertCtrl: AlertController,

    ) {
    }

    ionViewDidLoad() {

    }

    calc_res(){

        let io_alert:any;

        if(
            this.emp_income==undefined&&
            this.emp_extra_income==undefined
        ){
            io_alert = this.alertCtrl.create({
                title: "Notification",
                subTitle: "please add your income"
            });
            io_alert.present();

            return;
        }

        if(this.emp_income==undefined){
            this.emp_income=0;
        }

        if(this.emp_extra_income==undefined){
            this.emp_extra_income=0;
        }

        this.emp_income=parseInt(this.emp_income);
        this.emp_extra_income=parseInt(this.emp_extra_income);

        let annual_salary:any=0;
        let extra_annual_salary:any=0;

        if(this.emp_income_type=="annually"){
            annual_salary=this.emp_income;
        }
        else if(this.emp_income_type=="monthly"){
            annual_salary=this.emp_income*12;
        }
        else if(this.emp_income_type=="fortnightly"){
            annual_salary=this.emp_income*26;
        }
        else if(this.emp_income_type=="weekly"){
            annual_salary=this.emp_income*56;
        }

        if(this.emp_extra_income_type=="annually"){
            extra_annual_salary=this.emp_extra_income;
        }
        else if(this.emp_extra_income_type=="monthly"){
            extra_annual_salary=this.emp_extra_income*12;
        }
        else if(this.emp_extra_income_type=="fortnightly"){
            extra_annual_salary=this.emp_extra_income*26;
        }
        else if(this.emp_extra_income_type=="weekly"){
            extra_annual_salary=this.emp_extra_income*56;
        }

        this.navCtrl.push('CalcResPage',{
            'annual_salary':parseInt(annual_salary)+parseInt(extra_annual_salary),
            'financial_year':this.financial_year,
        });
    }


}
