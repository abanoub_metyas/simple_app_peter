
export class base_page{

    public static api_url(){
        // return "http://localhost/tareklancer/work/dev_ibrahim/food_delivery_2/food_delivery/public/index.php/";
        return "http://tareklancer.com/projects/dev_ibrahim/food_delivery/public/index.php/";
    }

    public static api_uploads_url(){
        // return "http://localhost/tareklancer/work/dev_ibrahim/food_delivery_2/food_delivery/public/upload/";
        return "http://tareklancer.com/projects/dev_ibrahim/food_delivery/public/upload/";
    }

    public static convert_json_to_arr(obj){
        return Object.keys(obj).map(function(k) { return obj[k] });
    }


}
