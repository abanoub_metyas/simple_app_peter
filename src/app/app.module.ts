import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {IonicPageModule} from 'ionic-angular';
import {MyApp} from './app.component';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';


@NgModule({
    declarations: [
        MyApp
    ],
    imports: [
        BrowserModule,
        IonicPageModule.forChild(MyApp),
        IonicModule.forRoot(MyApp, {
            tabsPlacement: 'top',
            platforms: {
                android: {
                    tabsPlacement: 'top'
                },
                ios: {
                    tabsPlacement: 'top'
                },
                windows: {
                    tabsPlacement: 'top'
                }
            }
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp
    ],
    providers: [
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        StatusBar
    ]
})
export class AppModule {
}
